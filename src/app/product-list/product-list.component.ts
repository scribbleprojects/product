import { Component, OnInit } from '@angular/core';
import {FormBuilder,Validators,FormGroup} from '@angular/forms';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  ProductForm:FormGroup;
  products =[
    {'id':'_001','name':'pen','category':'product1','price':'4','description':"product is made in india"},
    {'id':'_002','name':'eraser','category':'product1','price':'3','description':"product is made in india"},
    {'id':'_003','name':'book','category':'product1','price':'20','description':"product is made in india"},
    {'id':'_004','name':'notepad','category':'product1','price':'15','description':"product is made in india"},
    {'id':'_005','name':'paste','category':'product1','price':'10','description':"product is made in india"},
    {'id':'_006','name':'chair','category':'product2','price':'400','description':"product is made in india"},
    {'id':'_007','name':'table','category':'product2','price':'350','description':"product is made in india"}
  ]
  constructor(
    private fb:FormBuilder
  ) { }

  ngOnInit() {
    this.ProductForm =  this.fb.group({
      name:['',Validators.required],
      category:['',Validators.required],
      description:['',Validators.required],
      price:['',Validators.required],
    })
  }
  create()
{
  console.log(this.ProductForm.value,"fghjk")
}
edit(product){
  console.log(product,"ty")
}
delete(index)
{
  this.products.splice(index,1);
}
}
