import { Component } from '@angular/core';
import {ProductListComponent} from './product-list/product-list.component';

@Component({
  selector: 'app-root',
  template: '<app-product-list></app-product-list>',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'product';
}
